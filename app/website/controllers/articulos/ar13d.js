var mongoose = require('mongoose'),
    schema   = require('../../models/db/schema');

var ar13 = function(config){
  config = config || {};
  if(config.sol.body.actun == 'Actualizacion'){
    var dato = {
      year: config.sol.body.yearb,
      trimestre: config.sol.body.trimestreb,
      lugar: config.sol.files[0].destination,
      nombre: config.sol.files[0].filename,
      nombrer: config.sol.files[0].originalname
    }

    schema.ar13.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
      if(err){
        throw err;
      }
      config.res.json('=>');
    });
  }
  if(config.sol.body.actun == 'Actual'){
    if(config.sol.files[0] != undefined){
      var dato = new schema.ar13({
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: config.sol.files[0].destination,
        nombre: config.sol.files[0].filename,
        nombrer: config.sol.files[0].originalname
      });
    }else{
      var dato = new schema.ar13({
        year: config.sol.body.yearb,
        trimestre: config.sol.body.trimestreb,
        lugar: '',
        nombre: 'En este trimestre no se ha generado información al respecto',
        nombrer: ''
      });
    }

    
    schema.ar13.findOne({year:config.sol.body.yearb,trimestre:config.sol.body.trimestreb},(err,data)=>{
      if(err){
        throw err;
      }

      if(data == null){
        dato.save((err)=>{
          if(err){
            throw err;
          }
          config.res.json('=>');
        });
      }else{
        config.res.json('<=');
      }
    });
  }
  if(config.sol.body.actun == 'noinfo'){
    var dato = {
      year: config.sol.body.yearb,
      trimestre: config.sol.body.trimestreb,
      lugar: '',
      nombre: 'En este trimestre no se ha generado información al respecto',
      nombrer: ''
    }

    schema.ar13.update({year:dato.year,trimestre:dato.trimestre},dato,(err)=>{
      if(err){
        throw err;
      }
      config.res.json('=>');
    });
  }
}

module.exports = ar13;
