var mongoose = require('mongoose'),
    schema   = require('../../models/db/schema'),
    fs       = require('fs-extra'),
    pathc     = require('path');

var publicar = function(config){
  config = config || {};
  //******************Seccion A)*******************************//
  if(config.sol.body.estado == '1' || config.sol.body.estadono == '1'){
    var esquema = new schema.a({
      year: config.sol.body.yearReserva,
      trimestre:config.sol.body.trimestres,
      nombre: config.sol.body.noReserva,
      lugar: '',
      archivo: '',
      dato: config.sol.body.estado
    });

    schema.a.findOne({year:config.sol.body.yearReserva,trimestre:config.sol.body.trimestres},(err,data)=>{
      if(err){
        throw err;
      }

      if(data == null){
        esquema.save((err)=>{
          if(err){
            throw err;
          }
          config.res.json('=>');
        });
      }else{
        config.res.json('<=');
      }

    });

  }
  if(config.sol.body.estado == '0' || config.sol.body.estadono == '0'){
    var esquema = new schema.a({
      year: config.sol.body.yearReserva,
      trimestre:config.sol.body.trimestre,
      nombre: config.sol.body.nombreImg == undefined ? '' : config.sol.body.nombreImg,
      lugar: config.sol.file.path == undefined ? '' : config.sol.file.path,
      archivo: config.sol.file.filename == undefined ? '' : config.sol.file.filename,
      dato: config.sol.body.estado
    });
    
    schema.a.findOne({year:config.sol.body.yearReserva,trimestre:config.sol.body.trimestre},(err,data)=>{
      if(err){
        throw err;
      }
      if(data == null){
        esquema.save((err)=>{
          if(err){
            throw err;
          }
          config.res.json('=>');
        });
      }else{
        config.res.json('<=');
      }

    });
  }

  if(config.sol.body.estadoActualizado == '2'){
    if(config.sol.file){    

      schema.a.findOne({year:config.sol.body.actualizaYearReserva,trimestre:config.sol.body.trimestreActualiza},(err,data)=>{    
        if(err){
          throw err;
        }
        var esquema = {
          nombre: config.sol.body.nombreActualiza,
          lugar: config.sol.file.path,
          archivo: config.sol.file.filename,
          dato: config.sol.body.estadoActualizado
        };
        if(data == null){
          config.res.json('<=');
        }else{      
          schema.a.update({"year":config.sol.body.actualizaYearReserva,trimestre:config.sol.body.trimestreActualiza},esquema,(err)=>{
            if(err){
              throw err;
            }
            fs.move(pathc.join(__dirname,'../../../staticos/archivos/archivospdf/' + config.sol.file.filename),pathc.join(__dirname,'../../../staticos/archivos/archivospdf/' + config.sol.body.actualizaYearReserva+"/"+config.sol.body.trimestreActualiza + '/' + esquema.archivo),(err)=>{
              if(err){
                throw err;
              }
            });
            config.res.json('=>');
          });

        }
      });
    }else{

      schema.a.findOne({year:config.sol.body.actualizaYearReserva,trimestre:config.sol.body.trimestreActualiza},(err,data)=>{    
        if(err){
          throw err;
        }
        var esquema ={
          year: config.sol.body.actualizaYearReserva,
          trimestre:config.sol.body.trimestreActualiza,
          nombre: config.sol.body.nombreActualiza,
          lugar: '',
          archivo: '',
          dato: config.sol.body.estadoActualizado
        }
        if(data == null){
          config.res.json('<=');
        }else{      
          schema.a.update({year:config.sol.body.actualizaYearReserva,trimestre:config.sol.body.trimestreActualiza},esquema,(err)=>{
            if(err){
              throw err;
            }
            config.res.json('=>');
          });
        }
      });
    }

  }

}

module.exports = publicar;
