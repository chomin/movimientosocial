var session = require('express-session')
    mongodb = require('connect-mongodb-session')(session);

var mongoDBS = function(config){
  config = config || {};

  var store = new mongodb({
    uri: "mongodb://localhost/mov",
    collection: 'session'
  });

  store.on('error',(error)=>{
    assert.ifError(error);
    assert.ok(false);
  });

  return store;
}

module.exports = mongoDBS;
