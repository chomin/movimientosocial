var mongoose = require('mongoose'),
    schema   = require('../models/db/schema');

var rutas = function(config){
  config = config || {};

  config.app.get('/aw',(sol,res,next)=>{
    schema.a.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/a',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/a',{data:''});
      }
    });
  });

  config.app.get('/bw',(sol,res,next)=>{
    schema.b1.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/b',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/b',{data:''});
      }
    });
  });

  config.app.get('/bw2',(sol,res,next)=>{
    schema.b2.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/b2',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/b2',{data:''});
      }
    });
  });

  config.app.get('/bw3',(sol,res,next)=>{
    schema.b3.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        //
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/b3',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/b3',{data:''});
      }
    });
  });

  config.app.get('/bw4',(sol,res,next)=>{
    schema.b4.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/b4',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/b4',{data:''});
      }
    });
  });

  config.app.get('/bw5',(sol,res,next)=>{
    schema.b5.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/b5',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/b5',{data:''});
      }
    });
  });

  config.app.get('/bw6',(sol,res,next)=>{
    schema.b6.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/b6',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/b6',{data:''});
      }
    });
  });
  

  config.app.get('/cw',(sol,res,next)=>{
    schema.c.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/c',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/c',{data:''});
      }
    });
  });

  config.app.get('/dw',(sol,res,next)=>{
    schema.d.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/d',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/d',{data:''});
      }
    });
  });

  config.app.get('/ew',(sol,res,next)=>{
    schema.e.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/e',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/e',{data:''});
      }
    });
  });

  config.app.get('/fw',(sol,res,next)=>{
    schema.f.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/f',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/f',{data:''});
      }
    });
  });

  config.app.get('/gw',(sol,res,next)=>{
    schema.g.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/g',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/g',{data:''});
      }
    });
  });

  config.app.get('/hw',(sol,res,next)=>{
    schema.h.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/h',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/h',{data:''});
      }
    });
  });

  config.app.get('/iw',(sol,res,next)=>{
    schema.i.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/i',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/i',{data:''});
      }
    });
  });

  config.app.get('/jw',(sol,res,next)=>{
    schema.j.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/j',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/j',{data:''});
      }
    });
  });

  config.app.get('/kw',(sol,res,next)=>{
    schema.k.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/k',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/k',{data:''});
      }
    });
  });

  config.app.get('/lw',(sol,res,next)=>{
    schema.l.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/l',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/l',{data:''});
      }
    });
  });

  config.app.get('/mw',(sol,res,next)=>{
    schema.m.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/m',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/m',{data:''});
      }
    });
  });

  config.app.get('/nw',(sol,res,next)=>{
    schema.n.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/n',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/n',{data:''});
      }
    });
  });

  config.app.get('/ow',(sol,res,next)=>{
    schema.o.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/o',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/o',{data:''});
      }
    });
  });

  config.app.get('/pw',(sol,res,next)=>{
    schema.p.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/p',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/p',{data:''});
      }
    });
  });

  config.app.get('/qw',(sol,res,next)=>{
    schema.q.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/q',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/q',{data:''});
      }
    });
  });

  config.app.get('/rw',(sol,res,next)=>{
    schema.r.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/r',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/r',{data:''});
      }
    });
  });

  config.app.get('/sw',(sol,res,next)=>{
    schema.s.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/s',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/s',{data:''});
      }
    });
  });

  config.app.get('/tw',(sol,res,next)=>{
    schema.t.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/t',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/t',{data:''});
      }
    });
  });

  config.app.get('/ae12',(sol,res,next)=>{
    schema.ar12.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/ar12',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/ar12',{data:''});
      }
    });
  });
  config.app.get('/ae13',(sol,res,next)=>{
    schema.ar13.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/ar13',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/ar13',{data:''});
      }
    });
  });

  config.app.get('/ae14',(sol,res,next)=>{
    schema.ar14.find({},(err,data)=>{
      if(err){
        throw err;
      }
      var i;
      var yea = [];
      var primerTrimestre = []
      var segundoTrimestre = []
      var terceroTrimestre = []
      var cuartoTrimestre = []
      if(data[0] != undefined){
        
        var arrays = {
          year: yea,
          pr: primerTrimestre,
          se: segundoTrimestre,
          te: terceroTrimestre,
          cu: cuartoTrimestre
        }
        
        var we;
        for(i in data){
          if(yea.indexOf(data[i].year) <= -1){
            yea.push(data[i].year);
          }

          if(data[i].trimestre == "Primer Trimestre"){
            primerTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Segundo Trimestre"){
            segundoTrimestre.push(data[i]);
          }        
          if(data[i].trimestre == "Tercer Trimestre"){
            terceroTrimestre.push(data[i]);
          }
          if(data[i].trimestre == "Cuarto Trimestre"){
            cuartoTrimestre.push(data[i]);
          }
        }
        
        var fr = 3;
        res.render('transparencia/ar14',{data:arrays,fr:fr});
      }else{
        res.render('transparencia/ar14',{data:''});
      }
    });
  });
}

module.exports = rutas;
