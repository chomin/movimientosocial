import $ from 'jquery';

export function infoReservada(){
  if(localStorage['contenedor'] == 'inf-reser'){
    function subirInfoReservada(e){    
      var formData = new FormData($("#seccion-informacion")[0]);
      $.ajax('/informacionReservada',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#subiendo").html('Subiendo...');
        },
        success: (data)=>{
          console.log(data);
          $("#nombreImg").val('');
          $("#subiendo").html('');
        }
      });
      e.preventDefault();
    }

    function subirInfoRese(e){
      var datos = {
          trimestres: $("#trimestres").val(),
          estado: $("#estadono").val(),
          yearReserva: $("#year-reserva-no").val(),
          noReserva: "En este trimestre no se ha reservado información"
        }
      $.ajax('/informacionNoReservada',{
        type:'POST',
        dataType: 'json',
        data:datos,
        success: (data)=>{
          console.log(data);
          if(data == '=>'){
            $("#publicado").html('Se ha publicado el trimestre');
          }
          if(data == '<='){
            $("#publicado").html('Ya se estaba publicado este trimestre');
          }
        }
      });
      e.preventDefault();
    }

  function actualizarReservada(e){
    var datos = new FormData($("#actualizar-reservada")[0]);
    $.ajax('/actualizaReservada',{
      type: 'POST',
      dataType: 'json',
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      beforeSend:(data)=>{
        $("#esperando-actualizar").html('Actualizando...');
      },
      success:(data)=>{
        if(data == '=>'){
          $("#esperando-actualizar").html('Actualizado');
          $("#nombreActualiza").html('');
        }
        if(data == '<='){
          $("#esperando-actualizar").html('Ocurrio un error!');
          $("#nombreActualiza").html('');
        }
      }
    });

    e.preventDefault();
  }

   document.getElementById('seccion-informacion').addEventListener('submit',subirInfoReservada);
   document.getElementById('info-no-reser').addEventListener('submit',subirInfoRese);
   document.getElementById('actualizar-reservada').addEventListener('submit',actualizarReservada);
  }
}

export function b(){
  if(localStorage['contenedor'] == 'estruc-o-a-m-f-t-a-c-b'){
    function bs(e){    
      var formData = new FormData($("#formulario-in-b")[0]);
      $.ajax('/insb',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    function bs2(e){    
      var formData = new FormData($("#formulario-in-b2")[0]);
      $.ajax('/insb',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    function bs3(e){    
      var formData = new FormData($("#formulario-in-b3")[0]);
      $.ajax('/insb',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    function bs4(e){    
      var formData = new FormData($("#formulario-in-b4")[0]);
      $.ajax('/insb',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    function bs5(e){    
      var formData = new FormData($("#formulario-in-b5")[0]);
      $.ajax('/insb',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    function bs6(e){    
      var formData = new FormData($("#formulario-in-b6")[0]);
      $.ajax('/insb',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

   document.getElementById('formulario-in-b').addEventListener('submit',bs);
   document.getElementById('formulario-in-b2').addEventListener('submit',bs2);
   document.getElementById('formulario-in-b3').addEventListener('submit',bs3);
   document.getElementById('formulario-in-b4').addEventListener('submit',bs4);
   document.getElementById('formulario-in-b5').addEventListener('submit',bs5);
   document.getElementById('formulario-in-b6').addEventListener('submit',bs6);
  }
}

export function c(){
  if(localStorage['contenedor'] == 'c'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/cs',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function d(){
  if(localStorage['contenedor'] == 'd'){
    function ds(e){
      var formData = new FormData($("#formulario-in-d")[0]);
      $.ajax('/ds',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-d').addEventListener('submit',ds);
  }
}

export function es(){
  if(localStorage['contenedor'] == 'e'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/es',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function f(){
  if(localStorage['contenedor'] == 'f'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/fs',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function g(){
  if(localStorage['contenedor'] == 'g'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/gs',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function h(){
  if(localStorage['contenedor'] == 'h'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/hs',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function iss(){
  if(localStorage['contenedor'] == 'i'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/is',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function j(){
  if(localStorage['contenedor'] == 'j'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/js',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function k(){
  if(localStorage['contenedor'] == 'k'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/ks',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function l(){
  if(localStorage['contenedor'] == 'l'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/ls',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function m(){
  if(localStorage['contenedor'] == 'm'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/ms',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function n(){
  if(localStorage['contenedor'] == 'n'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/ns',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function o(){
  if(localStorage['contenedor'] == 'o'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/os',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function p(){
  if(localStorage['contenedor'] == 'p'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/ps',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function q(){
  if(localStorage['contenedor'] == 'q'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/qs',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function r(){
  if(localStorage['contenedor'] == 'r'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/rs',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function s(){
  if(localStorage['contenedor'] == 's'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/ss',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function t(){
  if(localStorage['contenedor'] == 't'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/ts',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function a12(){
  if(localStorage['contenedor'] == 'ar12'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/ar12s',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function a13(){
  if(localStorage['contenedor'] == 'ar13'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/ar13s',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}

export function a14(){
  if(localStorage['contenedor'] == 'ar14'){
    function cs(e){
      var formData = new FormData($("#formulario-in-c")[0]);
      $.ajax('/ar14s',{
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:(data)=>{
          $("#cargando-archivos").html('Subiendo espere porfavor...');
        },
        success: (data)=>{
          console.log(data);
          $("#cargando-archivos").html('Los archivos se han subido');
        }
      });
      e.preventDefault();
    }

    document.getElementById('formulario-in-c').addEventListener('submit',cs);
  }
}
