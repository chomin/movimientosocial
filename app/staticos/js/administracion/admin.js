import $ from 'jquery';
import moment from 'moment';
import { infoReservada, b, c, d, es, f, g, h, iss, j, k, l, m, n, o, p, q, r, s, t, a12, a13, a14} from './upload';
var fecha = new Date().getUTCFullYear();
export default ()=>{
  if(localStorage['lugar'] == 'admin'){
    function incisos(e){
      $.get('/' + e.target.id,(data)=>{
        $('#contenedor-lista-articulo').html(data);
        localStorage['contenedor'] = e.target.id;
        infoReservada();
        b();
        c();
        d();
        es();
        f();
        g();
        h();
        iss();
        j();
        k();
        l();
        m();
        n();
        o();
        p();
        q();
        r();
        s();
        t();
        a12();
        a13();
        a14();
      });
      var $d = $('.item-articlulos-at');
      e.preventDefault();
    }

    $(".item-articlulos-at").click(incisos);

  }
}
