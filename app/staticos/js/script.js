import $ from 'jquery';
import entrada from './entrada/entrada';
import login from './log/login';
import ad from './administracion/admin';
import transparenciaDatos from './transparencia/transparencia';

$(document).ready(()=>{
  entrada();
  login();
  ad();
  transparenciaDatos();
});

