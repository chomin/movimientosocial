import $ from 'jquery';

export default () =>{
  if(localStorage['lugar'] != 'admin'){  
    var altura = $('#menu').offset().top;

    $(window).on('scroll',function(){
      if($(window).scrollTop() > altura){
        $('#menu').addClass('menu-pegado');
      }else{
        $('#menu').removeClass('menu-pegado');
      }
    });
  }
}
