$(document).ready(function(){
  if(localStorage['contenedor'] == 'inf-reser'){
    function subirInfoReservada(e){    
      var formData = new FormData($("#seccion-informacion")[0]);
      $.ajax({
        url:'/informacionReservada',
        type:'POST',
        dataType: 'json',
        data:formData,
        cache: false,
        contentType: false,
        processData: false,
        success: (data)=>{
          console.log(data);
        }
      });
      e.preventDefault();
    }
   document.getElementById('seccion-informacion').addEventListener('submit',subirInfoReservada);
  }
});
