var express = require('express'),
    parser  = require('body-parser'),
    swig    = require('swig'),
    helmet  = require('helmet'),
    session = require('express-session'),
    staticos= require('./middlewares/staticos'),
    favicon = require('./middlewares/favicon'),
    rutas   = require('./website/views/rutas'),
    mongo   = require('./website/controllers/mongo'),
    mongoDBS= require('./website/controllers/mongodb'),
    ruteos  = require('./website/controllers/rutasAdmin'),
    secret  = process.env.SE,
    service = process.env.SER,
    dev     = process.env.dev;

function Server(config){
  config = config || {};
  var dbs = new mongoDBS();
  this.app = express();
  this.app.use(helmet());
  this.app.use(parser.json());
  this.app.use(parser.urlencoded({extended: true}));
  this.app.use(session({secret:secret,name:service,resave:false,saveUninitialized:false,store:dbs}));
  this.app.use(staticos);
  this.app.use(favicon);
  this.app.engine('html', swig.renderFile);
  this.app.set('view engine', 'html');
  this.app.set('views',__dirname + '/website/views/templetes');
  swig.setDefaults({cache:false,varControls:['¿¿','??']});
  if(dev = 'dev'){
    this.app.set('view cache', false);
  }

  var mongoDB = new mongo(this);

  var ruta = new rutas(this);

  var ruteo = new ruteos(this);

}

module.exports = Server;
