var cat = require('catlistener');

cat.server({
  enviroment:{
    dev: 'dev',
    SE: 'sepCres',
    SER: 'servCres'
  },
  node: 'supervisor',
  debug: '-debug',
  app: 'app'
});

cat.stylus({
  options: ['compiles','listener','watch'],
  css: './app/staticos/css',
  stylus: './app/staticos/stylus/estilo.styl'
});

cat.broserify({
  original: './app/staticos/js/script.js',
  compilado: './app/staticos/js/escript.js',
  presets: true
});
